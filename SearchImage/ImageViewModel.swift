//
//  SearchManager.swift
//  SearchImage
//
//  Created by Artem Soloviev on 06.02.2023.
//

import Foundation



@MainActor class ImageViewModel: ObservableObject {
    
    @Published  private(set) var imageArray: ImageModel?
    @Published var search: Bool = false
    
    func startSearchImage(searchString: String) async throws {
        DispatchQueue.main.async {
            self.search = true
        }
        defer {
            DispatchQueue.main.async {
                self.search = false
            }
        }
        
        guard let url = URL(string: "https://serpapi.com/search.json?engine=google&q=\(searchString)&tbm=isch&start=0&num=20&ijn=0&api_key=3a47217724ab0671b58a0d0cb3635427d2685498b735c981ffdd12ce3ce82d78".encodeUrl) else { print ("invalid URL")
            return
        }
        let (data, _) = try await URLSession.shared.data(from: url)
        do {
            let decodeData = try JSONDecoder().decode(ImageModel.self, from: data)
            DispatchQueue.main.async {
                self.imageArray = decodeData
            }
        } catch {
            print(error)
        }
    }
}
