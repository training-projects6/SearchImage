//
//  ImageResult.swift
//  SearchImage
//
//  Created by Artem Soloviev on 06.02.2023.
//

import Foundation

struct ImageModel: Codable {
    
    var imageResult: [ImageResult]
    
    enum CodingKeys: String, CodingKey {
        case imageResult = "images_results"
    }
}

struct ImageResult: Codable {
    
    var position: Int
    var thumbnail: String
    var source: String
    var title: String
    var link: String
    var original: String?
    var isProduct: Bool
    
    enum CodingKeys: String, CodingKey {
        case position, thumbnail, source, title, link, original
        case isProduct = "is_product"
    }
}
