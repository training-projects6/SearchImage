//
//  SwiftUIView.swift
//  SearchImage
//
//  Created by Artem Soloviev on 07.02.2023.
//

import SwiftUI

struct WebView: View {
      var result: ImageResult
    
    var body: some View {
        NavigationView {
            WebViewModel(url: URL(string: result.link)!)
        }
    }
}

//struct SwiftUIView_Previews: PreviewProvider {
//    static var previews: some View {
//        SwiftUIView()
//    }
//}
