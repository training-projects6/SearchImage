//
//  ContentView.swift
//  SearchImage
//
//  Created by Artem Soloviev on 05.02.2023.
//

import SwiftUI

struct ContentView: View {
    
    @State private var search = ""
    let columns = [GridItem(.adaptive(minimum: 140)) ]
    @StateObject var imageViewModel = ImageViewModel()
    
    var body: some View {
        NavigationView {
            VStack {
                if !imageViewModel.search {
                    if let imageArray = imageViewModel.imageArray{
                        ScrollView(.vertical) {
                            LazyVGrid(columns: columns) {
                                ForEach(imageArray.imageResult, id: \.link) { item in
                                    NavigationLink(destination: DetailView(result: item, array: imageArray)) {
                                        AsyncImage(url: URL (string: item.thumbnail)) { image in
                                            image
                                                .resizable()
                                            
                                        } placeholder: {
                                            ProgressView()
                                        }
                                        .frame(width: 170, height: 170, alignment: .center)
                                        .clipShape( RoundedRectangle(cornerRadius: 10))
                                    }
                                }
                            }
                            .padding()
                        }
                    }else {
                        Text("Ask me something")
                    }
                }else {
                    ProgressView()
                }
            }
            .searchable(text: $search)
            .onSubmit(of: .search) {
                Task {
                    do{
                        try await imageViewModel.startSearchImage(searchString: search)
                    } catch {
                        print(error)
                    }
                }
            }
            .navigationTitle("Search Image")
        }
    }
}
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
