//
//  Extention.swift
//  SearchImage
//
//  Created by Artem Soloviev on 06.02.2023.
//

import Foundation
import UIKit

extension String {
    var encodeUrl : String {
        return self.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
}
