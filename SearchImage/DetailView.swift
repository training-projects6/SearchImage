//
//  DetailView.swift
//  SearchImage
//
//  Created by Artem Soloviev on 06.02.2023.
//

import SwiftUI

struct DetailView: View {
    
    @State  var result: ImageResult
    var array: ImageModel
    
    @State private var showSheet = false
    
    var body: some View {
        NavigationView{
            VStack {
                AsyncImage(url: URL (string: result.thumbnail)) {image in
                    image
                        .resizable()
                        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
                        .aspectRatio(contentMode: .fit)
                        .clipShape( RoundedRectangle(cornerRadius: 10))
                        .padding()
                } placeholder: {
                    ProgressView()
                }
                Spacer()
                Button {
                    showSheet = true
                } label:  {
                    Text("Go to source ")
                        .padding()
                        .background(.blue)
                        .clipShape(RoundedRectangle(cornerRadius: 10))
                        .foregroundColor(.white)
                        .fontWeight(.medium)
                }
                .sheet(isPresented: $showSheet) {
                    WebView(result: result)
                }
                
                HStack {
                    Button{
                        result = prev(result, array: array)
                        
                    }label: {
                        Image(systemName: "arrow.left.circle.fill")
                            .resizable()
                            .frame(width: 50, height: 50)
                    }
                    Spacer()
                    Button{
                        result = next(result, array: array)
                    }label: {
                        Image(systemName: "arrow.right.circle.fill")
                            .resizable()
                            .frame(width: 50, height: 50)
                    }
                }
                .padding()
            }
        }
    }
    
    func next(_ result: ImageResult, array: ImageModel) -> ImageResult {
        for (index, value) in array.imageResult.enumerated(){
            if result.thumbnail == value.thumbnail && index + 1 != array.imageResult.count {
                return array.imageResult[index + 1]
            }
        }
        return result
    }
    
    func prev(_ result: ImageResult, array: ImageModel) -> ImageResult {
        for (index, value) in array.imageResult.enumerated(){
            if result.thumbnail == value.thumbnail && index + 0 != array.imageResult.count {
                return array.imageResult[index - 1]
            }
        }
        return result
    }
}

//struct DetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        DetailView()
//    }
//}
