//
//  SearchImageApp.swift
//  SearchImage
//
//  Created by Artem Soloviev on 05.02.2023.
//

import SwiftUI

@main
struct SearchImageApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
